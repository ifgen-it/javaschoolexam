package com.tsystems.javaschool.tasks.pyramid;


import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here


            // int[] A = {2, 9, 6, 6, 5, 7, 5, 1, 4, 8};
           // List<Integer> inList = Arrays.asList(2, 9, 6, 6, 5, 7, 5, 1, 4, 8);
            List<Integer> inList = inputNumbers;

            if(!isPyramid(inList.size())){
                //      System.out.println("Pyramidable");
                //    else {
                //      System.out.println("Not pyramidable");
                throw new CannotBuildPyramidException("Fail pyramid");
            }

            // ArrayList<Integer> inList = new ArrayList<Integer>();
            /*for(int n : A){
              Integer i = new Integer(n);
              inList.add(i);
            }*/
            try {
                Collections.sort(inList);
            }
            catch (NullPointerException e){
                throw new CannotBuildPyramidException("There is null-value in the list");
            }
            //     System.out.println(inList);

            ArrayList<ArrayList<Integer>> matr =
                    new ArrayList<ArrayList<Integer>>();

            int num = 1;
            int ind = 0;
            boolean exit = false;

            while(true){
                ArrayList<Integer> tempRow = new ArrayList<Integer>();
                for(int i=0;i< num;i++){
                    if(inList.size() == ind){
                        exit = true;
                        break;
                    }
                    int temp = inList.get(ind);
                    ind++;
                    tempRow.add(temp);
                }
                if(tempRow.size() > 0){
                    matr.add(tempRow);
                    num++;
                }

                if(exit) break;
            }

            //System.out.println(matr);

            //    System.out.println();
            int numZero = matr.size() -1;

            ArrayList<ArrayList<Integer>> matrOut =
                    new ArrayList<ArrayList<Integer>>();

            for( ArrayList<Integer> row : matr ){

                ArrayList<Integer> tempRow = new ArrayList<Integer>();
                for(int i=0; i<numZero; i++){
                    //    System.out.print("0 ");
                    tempRow.add(0);
                }

                for(int j=0; j<row.size(); j++){
                    //    System.out.print(row.get(j) + " ");
                    tempRow.add(row.get(j));
                    if(j < row.size() -1){
                        //     System.out.print("0 ");
                        tempRow.add(0);
                    }
                }

                for(int i=0; i<numZero; i++){
                    //    System.out.print("0 ");
                    tempRow.add(0);
                }
                //   System.out.println();
                matrOut.add(tempRow);
                numZero--;

            }

            //  System.out.println("matrOut:");
            // System.out.println(matrOut);

            int i = matrOut.size();
            int j = matrOut.get(0).size();

            int[][] matrRes = new int[i][];
            for(int k = 0; k < i; k++){
                matrRes[k] = new int[j];
            }

            for(int k = 0; k < i; k++){
                for(int l = 0; l < j; l++){
                    matrRes[k][l] = matrOut.get(k).get(l);
                }
            }

            // PRINT
            for(int k = 0; k < i; k++){
                for(int l = 0; l < j; l++){
                    //  System.out.print(matrRes[k][l] + " ");
                }
                //  System.out.println("");
            }
            return matrRes;

    }


    private static boolean isPyramid(int size){

        int rowSize = 1;

        while(size > 0){
            size -= rowSize;
            rowSize++;

        }

        if(size < 0) return false;
        else return true;
    }


}
