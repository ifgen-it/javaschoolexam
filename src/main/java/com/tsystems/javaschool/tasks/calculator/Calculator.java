package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.Locale;

class MyException extends Exception{
    public MyException(String s){
        super(s);
    }
}

class Num{
    public int id;
    public double val;

    @Override
    public String toString(){
        return id + " " + val;
    }
}

class Oper{

    public int idNum;
    public int prior;
    public int order;
    public String action;

    @Override
    public String toString(){
        return idNum + " " + action +" " + prior + ":" + order;
    }

}


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        String result = "";
        try{

            String in = statement;
            if(in == null || in.equals(""))
                throw new com.tsystems.javaschool.tasks.calculator.MyException("Input is empty!");

            //   System.out.println(in + "\n");

            ArrayList<com.tsystems.javaschool.tasks.calculator.Num> nums = new ArrayList<com.tsystems.javaschool.tasks.calculator.Num>();
            ArrayList<com.tsystems.javaschool.tasks.calculator.Oper> opers = new ArrayList<com.tsystems.javaschool.tasks.calculator.Oper>();

            int idNumCnt = 1;
            int priorHi = 10;
            int priorLow = 9;
            int parenthCnt = 0;

            String tempNum = "";
            boolean digFound = false;

            String prevCh = "0";

            for(int i=0;i<in.length();i++){
                String ch = in.substring(i, i+1);
                //System.out.println(ch + " ");

                if(ch.equals(" "))
                    throw new com.tsystems.javaschool.tasks.calculator.MyException("There is a gap at input!");

                else if(isDigit(ch) || ch.equals(".") || ch.equals(",")){
                    digFound = true;
                    tempNum += ch;

                    if(i == in.length()-1){
                        double n = Double.parseDouble(tempNum);
                        com.tsystems.javaschool.tasks.calculator.Num num = new com.tsystems.javaschool.tasks.calculator.Num();
                        num.id = idNumCnt++;
                        num.val = n;

                        nums.add(num);
                        digFound = false;
                        tempNum = "";
                    }
                }
                else {
                    if(digFound){
                        double n = Double.parseDouble(tempNum);
                        com.tsystems.javaschool.tasks.calculator.Num num = new com.tsystems.javaschool.tasks.calculator.Num();
                        num.id = idNumCnt++;
                        num.val = n;

                        nums.add(num);
                        digFound = false;
                        tempNum = "";
                    }

                    if(isOper(ch)) {
                        if(i == in.length()-1 || i == 0)
                            throw new com.tsystems.javaschool.tasks.calculator.MyException("Operator is at the begin or at the end of input!");

                        if(isOper(prevCh))
                            throw new com.tsystems.javaschool.tasks.calculator.MyException("More than 1 operator between operands");

                        com.tsystems.javaschool.tasks.calculator.Oper op = new com.tsystems.javaschool.tasks.calculator.Oper();
                        op.action = ch;
                        char c = ch.charAt(0);
                        if(c == '+' || c == '-')
                            op.prior = priorLow;
                        else if(c == '*' || c == '/')
                            op.prior = priorHi;
                        op.idNum = idNumCnt -1;

                        opers.add(op);

                    }
                    else if(ch.equals("(")){
                        parenthCnt++;
                        priorHi *= 2;
                        priorLow *= 2;
                    }
                    else if(ch.equals(")")){
                        parenthCnt--;
                        if(parenthCnt < 0)
                            throw new com.tsystems.javaschool.tasks.calculator.MyException("Error with parentheses");
                        priorHi /= 2;
                        priorLow /= 2;
                    }
                }  // else
                prevCh = ch;

            } // for

            if(parenthCnt != 0)
                throw new com.tsystems.javaschool.tasks.calculator.MyException("Number opened is not equals number closed parentheses");

            //   System.out.println(nums);
            //   System.out.println(opers);

            // GET ORDER
            TreeSet<Integer> priors = new TreeSet<Integer>();

            for(com.tsystems.javaschool.tasks.calculator.Oper o : opers){
                Integer i = new Integer(o.prior);
                priors.add(i);
            }
            // System.out.println(priors);

            int orderCnt = 1;
            ArrayList<com.tsystems.javaschool.tasks.calculator.Oper> sortOpers = new ArrayList<com.tsystems.javaschool.tasks.calculator.Oper>();

            ArrayList<Integer> lPriors = new ArrayList<Integer>(priors);
            // System.out.println(lPriors);
            for(int i= lPriors.size()-1; i >= 0; i--){
                Integer pr = lPriors.get(i);
                for(int j=0; j<opers.size();j++){
                    com.tsystems.javaschool.tasks.calculator.Oper op = opers.get(j);
                    if(op.prior == pr){
                        op.order = orderCnt++;
                        sortOpers.add(op);
                    }
                }
            }

            //    System.out.println(sortOpers);

            // CALCULATE
            for(com.tsystems.javaschool.tasks.calculator.Oper o : sortOpers){
                int leftInd = findNum(o.idNum, nums);
                int rightInd = leftInd + 1;

                double left = nums.get(leftInd).val;
                double right = nums.get(rightInd).val;

                double res = calc(left, right, o.action);
                com.tsystems.javaschool.tasks.calculator.Num r = nums.get(rightInd);
                r.val = res;
                nums.remove(leftInd);



            }
            result = String.format(Locale.ENGLISH, "%.4f", nums.get(0).val);
            result = result.replaceAll("([.])?(0)+$", "");

        }
        catch(NumberFormatException e){
            //  System.err.println(e.getMessage());
            result = null;
        }
        catch(com.tsystems.javaschool.tasks.calculator.MyException e){
            //   System.err.println(e.getMessage());
            result = null;
        }

        return result;
    }

    private static int findNum(int idN, ArrayList<com.tsystems.javaschool.tasks.calculator.Num> nums){
        for(int i=0; i<nums.size(); i++){
            com.tsystems.javaschool.tasks.calculator.Num n = nums.get(i);
            if(n.id == idN)
                return i;
        }
        return -1;
    }

    private static double calc(double l, double r, String act) throws com.tsystems.javaschool.tasks.calculator.MyException {
        if(act.equals("+"))
            return l+r;
        else if(act.equals("-"))
            return l-r;
        else if(act.equals("*"))
            return l*r;
        else if(act.equals("/")){
            if(r == 0)
                throw new com.tsystems.javaschool.tasks.calculator.MyException("Division by Zero!");
            return l/r;
        }
        else return 1;


    }

    private static boolean isDigit(String s){
        char ch = s.charAt(0);
        if(ch >= '0' && ch <= '9')
            return true;
        else
            return false;
    }

    private static boolean isOper(String s){
        char ch = s.charAt(0);
        if(ch == '+' || ch == '-' || ch == '*' || ch == '/')
            return true;
        else
            return false;
    }

}
