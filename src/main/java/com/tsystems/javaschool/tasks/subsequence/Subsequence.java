package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

     //   Object[] X = {1, 5, 6 , 0};
     //   Object[] Y = { 2, 1, 5, 4, 1, 6, 0};

      //  Object[] X = {1, 5, 6 , 0};
       // Object[] Y = { 2, 1, 5, 4, 1, 6, 0};

    //    ArrayList<Object> x = new ArrayList<Object>();
     //   ArrayList<Object> y = new ArrayList<Object>();

        if (x == null || y == null) throw new IllegalArgumentException("Null argument");

        List<Object> xObj = x;
        List<Object> yObj = y;

        /*for(Object s : X){
            x.add(s);
        }
        for(Object s : Y){
            y.add(s);
        }*/

     //   System.out.println("x: " + x);
     //   System.out.println("y: " + y);

        boolean res = isSubseq(xObj, yObj);

      //  System.out.println("\nres = " + res);

        return res;
    }

    static public boolean isSubseq(List<Object> x, List<Object> y){


        int indY = 0;
        int sizeY = y.size();

        for(Object sX : x){

            boolean found = false;
            for( ; indY < sizeY; indY++){
                Object tempY = y.get(indY);
                if(sX.equals(tempY)){
                    found = true;
                 //   System.out.print(sX + " ");
                    break;
                }
            }
            if(!found) return false;
        }
        return true;
    }
}
